class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def hello
  	render text: "hello, Kush Kumar Dhawan!"
  end

  def goodbye
  	render text: "Test 3 by kush kumar dhawan"

  end
end
